
## 1.0.5 [06-04-2024]

Update IAP version in asset documentation

See merge request itentialopensource/pre-built-automations/microsoft-teams-rest!25

2024-06-04 21:40:17 +0000

---

## 1.0.4 [04-29-2024]

Pre-Release for Certification of 2023.2 Release

See merge request itentialopensource/pre-built-automations/microsoft-teams-rest!22

2024-04-29 14:22:42 +0000

---

## 1.0.3 [04-26-2024]

Changes made at 2024.04.26_11:11AM

See merge request itentialopensource/pre-built-automations/microsoft-teams-rest!21

2024-04-26 15:14:25 +0000

---

## 1.0.2 [04-03-2024]

Regenerate documentation and metadata.json files

See commit 6608ce5

2024-03-18 15:14:19 +0000

---

## 1.0.1 [03-18-2024]

* Update metadata.json

See merge request itentialopensource/pre-built-automations/microsoft-teams-rest!19

---

## 1.0.0 [09-07-2023]

* Updates for new project framework with metadata and markdown files.

See merge request itentialopensource/pre-built-automations/microsoft-teams-notification!14

---
## 0.0.18 [06-24-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/microsoft-teams-notification!8

---

## 0.0.17 [01-05-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/microsoft-teams-notification!7

---

## 0.0.16 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/microsoft-teams-notification!6

---

## 0.0.15 [10-18-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/microsoft-teams-notification!5

---

## 0.0.14 [05-17-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/microsoft-teams-notification!1

---

## 0.0.13 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/microsoft-teams-notification!1

---

## 0.0.12 [05-05-2021]

* Update images/microsoft_teams_notification_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/microsoft-teams-notification!5

---

## 0.0.11 [03-23-2021]

* Update images/microsoft_teams_notification_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/microsoft-teams-notification!5

---

## 0.0.10 [03-02-2021]

* Update images/microsoft_teams_notification_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/microsoft-teams-notification!5

---

## 0.0.9 [03-02-2021]

* patch/2021-03-02T08-10-12

See merge request itential/sales-engineer/selabprebuilts/microsoft-teams-notification!4

---

## 0.0.8 [02-22-2021]

* Bug fixes and performance improvements

See commit 951c342

---

## 0.0.7 [02-05-2021]

* Bug fixes and performance improvements

See commit bee132f

---

## 0.0.6 [02-05-2021]

* Bug fixes and performance improvements

See commit e63a916

---

## 0.0.5 [02-05-2021]

* Bug fixes and performance improvements

See commit 5d5db02

---

## 0.0.4 [02-05-2021]

* Bug fixes and performance improvements

See commit bbbf59e

---

## 0.0.3 [02-05-2021]

* Bug fixes and performance improvements

See commit 43f9572

---

## 0.0.2 [01-29-2021]

* Bug fixes and performance improvements

See commit d5da6f7

---\n\n\n\n\n\n\n
