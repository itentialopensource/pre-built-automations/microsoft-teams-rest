# Overview 

Project for use cases around Microsoft Teams using REST protocol


## Workflows


<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Send Notification - Microsoft - Teams - REST</td>
      <td>Send notification on Microsoft teams using webhook key</td>
    </tr>
  </tbody>
</table>

For further technical details on how to install and use this Workflow Project, please click the Technical Documentation tab. 
